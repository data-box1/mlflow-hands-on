import random
import mlflow
from mlflow.pyfunc.model import PythonModel
# check if model was saved
import os.path
from os import path

class RandomPredictor(PythonModel):
  def __init__(self):
    pass

  def predict(self, context, model_input):
    return model_input.apply(lambda column: random.randint(0,1))

# Construct and save the model
model_path = "randomizer_model"
r = RandomPredictor()

# Check if model was previously saved
model_exists = path.exists("./randomizer_model")
if(not model_exists): 
  mlflow.pyfunc.save_model(path=model_path, python_model=r)

# Load the model in `python_function` format
loaded_model = mlflow.pyfunc.load_model(model_path)

import pandas as pd
model_input = pd.DataFrame([range(10)])

random_predictor = RandomPredictor()

# must be accompanied with the flag --experiment-name='stockpred_experiment_day5_up' in mlflow run
# Otherwise calls in Default experiment name
mlflow.set_experiment('stockpred_experiment_day5_up')
with mlflow.start_run():
    model_output = loaded_model.predict(model_input)

    mlflow.log_metric("Days Up",model_output.sum())
    mlflow.log_artifact("train.py")
    
print(model_output)

    