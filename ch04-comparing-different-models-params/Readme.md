We compare these models:

1) Logistic Classifier: Part of the family of linear-based models and a commonly
used baseline.

2) Xgboost: This belongs to the family of tree boosting algorithms where many weak
tree classifiers are assembled to produce a stronger model.

3) Keras: This type of model belongs to the neural network's family and is generally indicated for situations where there is a lot of data available and relations are not linear between the features.

Follow the notebooks while in other terminal run ui.sh & check the mlflow GUI locally at http://127.0.0.1:5001 (you can change the port by changing in the script the --port flag)