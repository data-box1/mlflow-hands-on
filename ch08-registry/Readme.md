## Chapter 8: Training models w MLFlow & Registry

The training workflow:

Training data > Training Job > Evaluate Model > Deploy Model in Registry > Model Registry (MLFlow)

The next step will be to explore how  the Model Registry can be consumed by Inference API or Batch Scoring

### Setup postgresql:
```python
# Install libs
sudo apt install libpq-dev
# Start the service
service postgresql start
# Create user admin
sudo -u postgres createuser admin
# Create db mlflow
sudo -u postgres createdb mlflow
# Postgres terminal
sudo -u postgres psql
# Change user pass
in the postgres-# terminal type: \password admin
```

Configure your env variables in  `env_mlflow` according to your setup.

### Run the project:

Open a terminal, load the environment file `source env_mlflow` and run the tracking server `01_tracking_server.sh`. In other terminal you can run the project `02_evaluate_model.sh`. To see the results you can open the tracking server gui in the navigator at the uri `http://127.0.0.1:8001`. Optionally, to continue using the mlflow gui, open a new terminal and load the environment file `source env_mlflow` and run (`03_ui.sh`). You will see the same info as opening the uir of the tracking server.

### Additional readings:

* [Introduction to MLflow for MLOps Part 3: Database Tracking, Minio Artifact Storage, and Registry](https://medium.com/noodle-labs-the-future-of-ai/introduction-to-mlflow-for-mlops-part-3-database-tracking-minio-artifact-storage-and-registry-9fef196aaf42)

* [Tutorial MLflow: MLOps de forma sencilla](https://anderfernandez.com/blog/tutorial-mlflow-completo/)