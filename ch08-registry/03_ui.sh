# Be sure you have mlflow-hans-on env loaded
# Configured & type source env_mlflow
# Run localy the tracking server. Open the GUI at http://127.0.0.1:5001

mlflow ui --backend-store-uri postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${MLFLOW_BACKEND_STORE}:${POSTGRES_PORT}/${POSTGRES_DATABASE} --default-artifact-root  ${MLFLOW_ARTIFACT_STORE} -h ${MLFLOW_UI_HOST} -p ${MLFLOW_UI_PORT}

    

