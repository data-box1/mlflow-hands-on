#Not being used
import mlflow

if __name__ == "__main__":
    
    with mlflow.start_run(run_name="register_model") as run:

        mlflow.set_tag("mlflow.runName", "register_model")
        result = mlflow.register_model(
           "runs:/b5db1cc3ab854fbc82de2f7ac061f72e/artifacts/model",
            "training-model-psystock")
