# Data and Feature Management project component
Adapted from [Machine Learning Engineering with MLflow (Packt) git repo](https://github.com/PacktPublishing/Machine-Learning-Engineering-with-MLflow/blob/master/Chapter07/psystock-data-features-main/check_verify_data.py)

#### Highlights Notes taken from the book:

From a data quality perspective, in a dataset there are a couple of critical dimensions with which to assess and profile our data, namely:

• Schema compliance: Ensuring the data is from the expected types; making sure
that numeric values don't contain any other types of data

• Valid data: Assessing from a data perspective whether the data is valid from a
business perspective

• Missing data: Assessing whether all the data needed to run analytics and algorithms is available

#### Run the examples:

python main.py

# How to implement Feast 

[How to add a new dataset to the Feast feature store](https://www.mikulskibartosz.name/adding-datasets-to-feast-feature-store/)

[Made With ML- Feature Store](https://colab.research.google.com/github/GokuMohandas/MLOps/blob/main/notebooks/feature_store.ipynb#scrollTo=LPZmAUydQIC9)



