# MLflow Hands-on: Machine Learning Engineering with MLflow's book

An adapted/tested version of the code from [Machine Learning Engineering with MLflow](https://github.com/PacktPublishing/Machine-Learning-Engineering-with-MLflow). Here I focused on learning the grounds of MLFlow with the purpose of integrating the framework to ML projects. 

### Quick book's Review:
Good introductory book to mlflow software & basics of ml architecture (6/10 stars): The first and second part were very long for me, there are sections or explanations that are repeated. I would compress those section in just one with three/four more concise chapters, culminating with the analysis & dicussion of hyperparameters and the different studied models. The last section of each chapter (further reading) is not well connected with the text. In chapter 5, where custom models begin to be explained only a very simple example is shown.  I'd have prefered a complete example and the corresponding analysis of the model in the framework.
The book becomes very interesting from section 3 where it describes and analyzes some aspects of an architecture suitable for machine learning projects. It was interesting the use of the libraries: "great expectation" & "Feast" in the data layer and featuring.
+ comments

#### This repo contains:

* Modified/commented code samples discussed in the book

* Original container jupyter, mlflow & posgress

#### Setup environment:

1)  Download & install [Conda](https://github.com/conda/conda) 

2)  Create a base environment to running the project
conda env create -f requeriments.yml

#### Run the examples:

* Activate the mlflow-hans-on conda env

* MLProject examples can be run by the provided shell script run.sh & view/analyse w/ ui.sh script

* Some parts of the project are in notebook format so you can run typing jupyter notebook and open the .ipynb file

#### Complementary readings:

* [Machine Learning Model Development and Deployment with MLflow and Scikit-learn Pipelines](https://towardsdatascience.com/machine-learning-model-development-and-deployment-with-mlflow-and-scikit-learn-pipelines-f658c39e4d58)

#### References:

[Machine Learning Engineering with MLflow (Packt)](https://www.packtpub.com/product/Machine-Learning-Engineering-with-MLflow/9781800560796?utm_source=github&utm_medium=repository&utm_campaign=9781800560796)